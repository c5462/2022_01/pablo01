tableextension 50100 "PSRCustomerExtC01" extends Customer
{

    fields
    {
        field(50100; PSRNumVacunasC01; Integer)
        {
            Caption = 'Nº de vacunas';
            FieldClass=FlowField;
            Editable=false;
            CalcFormula = count(PSRLinPlanVacunacionC01 where("Codigo Cliente a Vacunar"=field("No.")));
        }
        field(50101; PSRFechaUltimaVacunaC01; Date)
        {
            Caption = 'Fecha última vacuna';
            FieldClass=FlowField;
            Editable=false;
            CalcFormula = max(PSRLinPlanVacunacionC01.FechaVacuna where("Codigo Cliente a Vacunar"=field("No.")));
            
        }
        field(50102; PSRTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de vacuna';
            DataClassification = CustomerContent;
            TableRelation = PSRVariosC01.PSRCodigoC01 where(PSRTipoC01 = const(Vacuna));

            trigger OnValidate()
            var
            rlPSRVariosC01: Record  PSRVariosC01;

            begin
                if rlPSRVariosC01.Get(rlPSRVariosC01.PSRTipoC01::Vacuna,Rec.PSRTipoVacunaC01) then begin
                    rlPSRVariosC01.TestField(Bloqueado,false);
                end;
            end;
        
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
            culPSRVblesGlobalesAppC01: Codeunit PSRVblesGlobalesAppC01;
            begin
                culPSRVblesGlobalesAppC01.NombreF(Rec.Name);
            end;
        }

    }

}
