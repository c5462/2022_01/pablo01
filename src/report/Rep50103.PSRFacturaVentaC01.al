report 50103 "PSRFacturaVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'facturaventa.rdlc';

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            column(No_Sales; "No.")
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xShipAdrr1; xShipAdrr[1])
            {

            }
            column(xShipAdrr2; xShipAdrr[2])
            {

            }
            dataitem(Integer; Integer)
            {


                column(Number_Integer; Number)
                {
                }
            }

            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                // DataItemLink = "Document No." = field("No.");

                column(LineNo_SalesInvoiceLine; "Line No.")
                {
                }
            }
            trigger onpredataitem()

            begin


            end;


            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                //     cuFormatAdress.salesInvSellTo(xCustAddr, "Sales Invoice Header");
                //     cuFormatAdress.salesInvSellTo(xShipAdrr, xCustAddr, "Sales Invoice Header");
                //     rCompanyInfo.get;
                //     cuFormatAdress.Company(xCompanyAdrr, rCompanyInfo);

            end;

        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // // field(xcopias; xcopias)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        xCustAddr: array[8] of Text;
        xShipAdrr: array[8] of text;
    // xcompanyAdrr: array[8] of text;
    // rCompanyInfo: Record "Company Information";
    // cuFormatAdress: Codeunit For;
    // xcopias: Integer; 'xcopias';

}