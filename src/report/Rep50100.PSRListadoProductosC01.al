report 50100 "PSRListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'Listadoproductos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Venta/Compras ';
    Caption = 'Listado Productos Venta/Compras mal';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.","Item Category Code";

            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                DataItemLink="No."=field("No.");
                RequestFilterFields = "Posting Date";

                column(Quantity_Facturaventa; Quantity)
                {
                }
                column(UnitPrice_Facturaventa; "Unit Price")
                {
                }
                column(AmountIncludingVAT_Facturaventa; "Amount Including VAT")
                {
                }
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(NoCliente; rCustomer."No.")
                {

                }
                column(PostingDate_Facturaventa; "Posting Date")
                {
                }
                //trigger OnPostDataItem()
                //begin
                    //Facturaventa.SetRange("No.",Productos."No.");
                //end;


                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;

                    // rCustomer.SetRange("No.",Facturaventa."Sell-to Customer No.");
                    // rCustomer.SetFilter("No.",Facturaventa."Sell-to Customer No." + '..' + Facturaventa."Sell-to Customer No." );
                    // rCustomer.FindFirst()
                end;

            }
            dataitem(Facturacompras; "Purch. Inv. Line")
            {
                 DataItemLink="No."=field("No.");
                 RequestFilterFields = "Posting Date";

                column(Quantity_Facturacompras; Quantity)
                {
                }
                column(UnitCost_Facturacompras; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_Facturacompras; "Amount Including VAT")
                {
                }
                column(NombreProveedor; rVendor.Name)
                {

                }
                column(NoProveedor; rVendor."No.")
                {

                }
                column(PostingDate_Facturacompras; "Posting Date")
                {
                }
                //trigger OnPostDataItem()
                //begin
                    //Facturacompras.SetRange("No.",Productos."No.");
                //end;

                trigger OnAfterGetRecord()

                begin
                    if not rVendor.Get(Facturacompras."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }
        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}