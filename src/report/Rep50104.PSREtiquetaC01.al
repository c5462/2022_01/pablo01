report 50104 "PSREtiquetaC01"
{
    Caption = 'Etiqueta';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout=RDLC;
    RDLCLayout= 'etiqueta.rdlc';
    UseRequestPage=false;
    
    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {

            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            column(No_SalesShipmentLine; "No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }
            dataitem(Integer;Integer)
            {

                column(Number_Integer; Number)
                {
                }
                trigger OnPreDataItem()
                begin
                    integer.SetRange(Number,1,"Sales Shipment Line".Quantity);
                    
                end;
            }
        }
    }
    
    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }
}