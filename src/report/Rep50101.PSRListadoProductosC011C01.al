report 50101 "PSRListadoProductosC011C01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'listadoprocudtos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Ventas/Compras';
    Caption = 'Listado Productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";
            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                DataItemLink = "No." = field("No.");

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                column(NombreCliente; rcustomer.Name)
                {

                }
                column(NoCliente; rcustomer."No.")
                {

                }
                trigger OnPreDataItem()
                begin
                    //FacturaVenta.SetRange("No.",Productos."No.");
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;

                    // rCustomer.SetRange("No.",FacturaVenta."Sell-to Customer No.");
                    // rCustomer.SetFilter("No.",FacturaVenta."Sell-to Customer No." + '&' + FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst()
                end;
            }
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                RequestFilterFields = "Posting Date";
                DataItemLink = "No." = field("No.");

                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                column(NombreProveedor; rVendor.Name)
                {

                }
                column(NoProveedor; rVendor."No.")
                {

                }
                trigger OnAfterGetRecord()
                begin
                    if not rVendor.Get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }

        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}