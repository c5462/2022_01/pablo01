report 50107 "PSRGenericoVentasC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'genericoventas.rdlc';
    DefaultLayout = RDLC;
    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    PSRLinGenericaVentasC01.Init();
                    PSRLinGenericaVentasC01.TransferFields("Sales Invoice Line");
                    PSRLinGenericaVentasC01.Insert(false);

                    



                end;
            }
            trigger OnPreDataItem()
            begin
                if "Sales Invoice Header".GetFilters() = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                PSRCabeceraGenericaVentasC01.Init();
                PSRCabeceraGenericaVentasC01.TransferFields("Sales Invoice Line");
                PSRCabeceraGenericaVentasC01.Insert(false);

                xcaption :='Factura';

            end;
            
        }
        dataitem("Sales Shipment Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Shipment Line"; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    PSRLinGenericaVentasC01.Init();
                    PSRLinGenericaVentasC01.TransferFields("Sales Invoice Line");
                    PSRLinGenericaVentasC01.Insert(false);


                end;
            }
            trigger OnPreDataItem()
            begin
                if "Sales Shipment Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                PSRCabeceraGenericaVentasC01.Init();
                PSRCabeceraGenericaVentasC01.TransferFields("Sales Invoice Line");
                PSRCabeceraGenericaVentasC01.Insert(false);

                xcaption :='Albaran';

            end;

        }
        dataitem(PSRCabeceraGenericaVentasC01; PSRCabeceraGenericaVentasC01)
        {
            RequestFilterFields = "No.";
            column(No_PSRCabeceraGenericaVentasC01; "No.")
            {
            }
            column(SelltoCustomerNo_PSRCabeceraGenericaVentasC01; "Sell-to Customer No.")
            {
            }
            column(PostingDate_PSRCabeceraGenericaVentasC01; "Posting Date")
            {
            }
            column(xcaption;xcaption)
            {
            }
            dataitem(PSRLinGenericaVentasC01; PSRLinGenericaVentasC01)
            {
                DataItemLink = "Document No." = field("No.");
                column(No_PSRLinGenericaVentasC01; "No.")
                {
                }
                column(Description_PSRLinGenericaVentasC01; Description)
                {
                }
                column(Quantity_PSRLinGenericaVentasC01; Quantity)
                {
                }
                trigger OnAfterGetRecord()
                begin

                end;
            }
            trigger OnAfterGetRecord()
            begin

            end;

        }
    }

    procedure GetAlgo()
    var
        myInt: Integer;
    begin

    end;

    procedure SetTipoInforme(pOption: Option Factura,albaran)
    var
        myInt: Integer;
    begin

    end;

    var
        myInt: Integer;
        xopcion: Option Factura,albaran;

        xcaption: Text;

}