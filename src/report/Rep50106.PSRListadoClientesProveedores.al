report 50106 "PSRListadoClientesProveedores"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'listadoClientesProveedores.rdlc';
    AdditionalSearchTerms = 'Listado Clientes/Proveedores';
    Caption = 'Listado clientes/Proveedores';

    dataset
    {
        dataitem(Cliente; Customer)
        {
            RequestFilterFields = "No.";
            
            column(No_Cliente; "No.")
            {
            }
            column(Name_Cliente; Name)
            {
            }
            column(City_Cliente; City)
            {
            }
            column(rCompanyInformation;rCompanyInformation.Name)
            {
            }


            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                DataItemLink =  "Sell-to Customer No."= field("No.");

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }

            }
        }
            dataitem(Proveedor; Vendor)
            {

                column(No_Proveedor; "No.")
                {
                }
                column(Name_Proveedor; Name)
                {
                }
                column(City_Proveedor; City)
                {
                }

                dataitem(FacturaCompra; "Purch. Inv. Line")
                {
                    RequestFilterFields = "Posting Date";
                    DataItemLink = "Buy-from Vendor No." = field("No.");

                    column(Quantity_FacturaCompra; Quantity)
                    {
                    }
                    column(DocumentNo_FacturaCompra; "Document No.")
                    {
                    }
                    column(UnitCost_FacturaCompra; "Unit Cost")
                    {
                    }
                    column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                    {
                    }
                    column(PostingDate_FacturaCompra; "Posting Date")
                    {
                    }
                }
            }



    }
    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                // action(ActionName)
                // {
                //     ApplicationArea = All;

                // }
            }
        }
    }
    trigger OnPreReport()
    begin
        if not rCompanyInformation.get() then begin
            Clear(rCompanyInformation);
        end;
        
    end;

 var
 rCompanyInformation: Record  "Company Information";
}