report 50102 "PSRlistadoproyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'listadoproyectos.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(Job; Job)
        {
            RequestFilterFields = "No.", "Creation Date", Status;
            // DataItemTableView = sorting("No.")

            column(NoProyecto; "No.")
            {
            }
            column(Descripcionproyecto; Description)
            {
            }
            column(cliente; "Bill-to Customer No.")
            {
            }
            column(fecha; "Creation Date")
            {
            }
            column(nombrecliente; "Bill-to Name")
            {
            }
            column(Status_DataItemName; Status)
            {
            }
            column(Xcolor; xcolor)
            {
            }

            dataitem("Job Task"; "Job Task")
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");

                column(JobTaskNo_JobTask; "Job Task No.")
                {
                }
                column(Description_JobTask; Description)
                {
                }
                column(StartDate_JobTask; Format("Start Date"))
                {
                }
                column(EndDate_JobTask; "End Date")
                {
                }
                column(JobTaskType_JobTask; "Job Task Type")
                {
                }
                dataitem("Job Planning Line"; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");

                    column(No_JobPlanningLine; "No.")
                    {
                    }
                    column(Description_JobPlanningLine; Description)
                    {
                    }
                    column(Quantity_JobPlanningLine; Quantity)
                    {
                    }
                    column(PlanningDate_JobPlanningLine; Format("Planning Date"))
                    {
                    }
                    column(LineAmount_JobPlanningLine; "Line Amount")
                    {
                    }
                    column(Type_JobPlanningLine; "Type")
                    {
                    }
                    column(LineNo_JobPlanningLine; "Line No.")
                    {
                    }
                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");


                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {
                        }
                        column(DocumentDate_JobLedgerEntry; Format("Document Date"))
                        {
                        }
                        column(Type_JobLedgerEntry; "Type")
                        {
                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {
                        }
                    }
                }
            }
            trigger OnAfterGetRecord()
            begin

                case Job.Status of
                    job.Status::Completed:
                        begin
                            xcolor := 'Green'
                        end;
                    job.Status::Open:
                        begin
                            xcolor := 'Blue'
                        end;
                    job.Status::Planning:
                        begin
                            xcolor := 'Yellow'
                        end;
                    job.Status::Quote:
                        begin
                            xcolor := 'Red'
                        end;



                end
            end;





        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }


    }

    var
        // myInt: Integer;
        xcolor: Text[20];
}