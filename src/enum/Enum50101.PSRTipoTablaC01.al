enum 50101 "PSRTipoTablaC01"
{


    value(0; Cliente)
    {
        Caption = 'Tabla de Cliente';
    }
    value(10; Proveedor)
    {
        Caption = 'Proveedor';
    }
    value(20; Recurso)
    {
        Caption = 'Recurso';
    }
    value(30; Empleado)
    {
        Caption = 'Empleado';
    }
    value(40; Contacto)
    {
        Caption = 'Contacto';
    }
}
