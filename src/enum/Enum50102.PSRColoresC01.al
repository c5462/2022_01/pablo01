enum 50102 "PSRColoresC01"
{
    Extensible = false;

    value(0; Blanco)
    {
        Caption = 'Blanco';
    }
    value(1; Rojo)
    {
        Caption = 'Rojo';
    }
    value(2; Gris)
    {
        Caption = 'Gris';
    }
    value(3;Negro)
    {
        Caption = 'Negro';
    }
}
