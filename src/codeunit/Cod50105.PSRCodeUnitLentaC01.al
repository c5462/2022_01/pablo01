codeunit 50105 "PSRCodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;

    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Seguro que quiere hacer este proceso?');

        end;
        if xlEjecutar then begin
            InsertarLogF('Tarea iniciada');
            Commit();//No usar casi nunca
            Message('En proceso');
            Sleep(10000);
            InsertarLogF('Tarea finalizada');
        end;
    end;

    local procedure InsertarLogF(pMensaje: Text)
    var
        rlPSRLogC01: Record PSRLogC01;
    begin
        rlPSRLogC01.Init();
        rlPSRLogC01.Insert(true);
        rlPSRLogC01.Validate(Mensaje, pMensaje);
        rlPSRLogC01.Modify(true);
    end;

}