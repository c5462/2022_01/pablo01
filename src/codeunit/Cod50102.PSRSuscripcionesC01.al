codeunit 50102 "PSRSuscripcionesC01"
{
    SingleInstance = true;

   
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'URL1', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventF(CurrFieldNo: Integer; var xRec: Record "Record Link")
    begin

    end;

    // [EventSubscriber(ObjectType::Page, Page::"Customer Card", 'OnQueryClosePageEvent','' , false, false)]
    // local procedure PageCustomerCardOnQueryClosePageEventF()
    // begin

    // end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        culPSRFuncionesAppC01: Codeunit PSRFuncionesAppC01;

    begin
       culPSRFuncionesAppC01.MensajeFraCompraF(PurchInvHdrNo);
    end;

    





}