codeunit 50101 "PSRVblesGlobalesAppC01"
{
    SingleInstance = true;
    procedure SetSwitchF(pvalor: Boolean)
    begin
        xSwitch := pValor;
        
    end;

    procedure GetSwitchF() return: Boolean
    begin
        return := xSwitch;

    end;

    /// <summary>
    /// Guarda el nombre a nivel global a BC
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        xName := pName;
    end;
    /// <summary>
    /// Devuelve el nmombre guardado globalmente
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin   
        exit(xName);
    end;

    var
        xSwitch: Boolean;
        xName: Text;
}