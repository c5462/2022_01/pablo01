codeunit 50104 "PSRFuncionesAppC01"
{
    procedure MensajeFraCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('%1 ha registrado la factura: %2', UserId, PurchInvHdrNo);
        end;
    end;

    procedure FuncionEjemploC01F(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) return: Text
    var
        i: Integer;
    begin

    end;

    procedure EjemploVariablesESF(var pTexto: Text)
    begin
        pTexto := 'Tecon  Servicios';
    end;

    procedure EjemploVariablesESF02(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon  Servicios');
    end;

    procedure EjemploArrayF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
        xlNumelmentosArray: Integer;
    begin
        Randomize(1);
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := format(Random(ArrayLen(mlNumeros)));
        end;
        xlNumelmentosArray := CompressArray(mlNumeros);
        Message('La bola que ha salido es %1');


    end;

    procedure BingoF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
        xlNum: Integer;
    begin
        Randomize(1);
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := format(i);
        end;

        repeat
            xlNum := Random(CompressArray(mlNumeros));
            Message(mlNumeros[xlNum]);
            mlNumeros[xlNum] := '';
        until CompressArray(mlNumeros) <= 0;
    end;

    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;
    procedure EjemploSobreCargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobreCargaF(p1,p2,p3,false);
    end;


    procedure EjemploSobreCargaF(p1: Integer; p2: Decimal; p3: Text; pBooleano: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 12.45;
        p3 := 'Lo que sea';
        //Nueva funcionalidad
        if pBooleano then begin
            xlNuevoNumero := p1 * p2;
        end;

    end;

    procedure UsoFuncionEjemplo()
    begin
        EjemploSobreCargaF(10,34.56,'Hola Mundo');
        EjemploSobreCargaF(0,234.56,'Mundo cruel');

    end;

    procedure SintaxisF();
    var
    i: Integer;
        xlTexto: Text;
    begin
        i := 1;
        i := i +10;
        i := 10;
        xlTexto := 'Pablo';
        xlTexto := xlTexto + 'Sarrión';
        xlTexto := 'Romero';

    end;

}