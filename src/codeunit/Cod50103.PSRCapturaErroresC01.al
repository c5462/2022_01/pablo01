codeunit 50103 "PSRCapturaErroresC01"
{
    trigger OnRun()
    var
        rlcustomer: Record customer;
        clCodeCte: Label 'ABC', Locked = true, Comment = 'El codigjsdnvod';


    begin
        if not rlcustomer.get(clCodeCte) then begin
            rlcustomer.Init();
            rlcustomer.Validate("No.", clCodeCte);
            rlcustomer.Insert(true);
            rlcustomer.Validate(Name, 'Proveedor de prueba');
            rlcustomer.Validate(Address, 'Mi calle');
            rlcustomer."VAT Registration No." := 'xxxxx';// PAra que no se verifique si el cif esta en otro proveedor
            rlcustomer.Validate("Payment Method Code", 'PSR');
            rlcustomer.Modify(true)
        end;
        Message('Datos del cliente: %1', rlcustomer);
    end;

    procedure FuncionParaCapturarErrorF(pCodProveedor: Code[20]): Boolean
    var
        rlvendor: Record vendor;
    begin
        if not rlvendor.get(pCodProveedor) then begin
            rlvendor.Init();
            rlvendor.Validate("No.", pCodProveedor);
            rlvendor.Insert(true);
            if AsignarDatosProveedor(rlvendor) then begin
                rlvendor.Modify(true)
            end else begin
                Message('Error en asignar datos (%1)', GetLastErrorText());
            end;
        exit(true)
        end;
        Message('Ya existe el proveeedor %1',pCodProveedor);
    end;

    [TryFunction]
    local procedure AsignarDatosProveedor(var rlvendor: Record vendor)
    begin
        rlvendor.Validate(Name, 'Proveedor de prueba');
        rlvendor.Validate(Address, 'Mi calle');
        rlvendor."VAT Registration No." := 'xxxxx';// Para que no se verifique si el cif esta en otro
        rlvendor.Validate("Payment Method Code", 'PSR');
    end;
}