pageextension 50100 "PSRCustomerCartExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(PSRVacunasC01)
            {
                Caption = 'Vacunas';


                field(PSRNumVacunasC01; Rec.PSRNumVacunasC01)
                {
                    ToolTip = 'Specifies the value of the Nº de vacunas field.';
                    ApplicationArea = All;

                }

                field(PSRFechaUltimaVacunaC01; Rec.PSRFechaUltimaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha última vacuna field.';
                    ApplicationArea = All;

                }

                field(PSRTipoVacunaC01; Rec.PSRTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de vacuna field.';
                    ApplicationArea = All;



                }
            }


        }

    }
    actions
    {
        addlast(processing)

        {

            action(PSRAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                caption = 'Asignar vacuna bloqueada';

                trigger OnAction()
                begin
                    rec.Validate(PSRTipoVacunaC01, 'BLOQUEADA');
                end;
            }
            action(PSRSwitchVariableC01)
            {
                ApplicationArea = all;
                captioN = 'Prueba variables Globales a BC';
                trigger OnAction()
                var
                    clMensaje: Label 'Es valor actual es %1. Lo cambimaos a %2';
                begin
                    Message(clMensaje, culPSRFuncionesAppC01.GetSwitchF(), not culPSRFuncionesAppC01.GetSwitchF());
                    culPSRFuncionesAppC01.SetSwitchF(not culPSRFuncionesAppC01.GetSwitchF());

                end;
            }
        }
    }
    var
        culPSRFuncionesAppC01: Codeunit "PSRVblesGlobalesAppC01";



    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta página?', false));

    end;



}
