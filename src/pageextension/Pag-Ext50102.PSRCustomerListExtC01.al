pageextension 50102 "PSRCustomer ListExtC01" extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(PSRSwitchVariableC01)
            {
                ApplicationArea = all;
                captioN = 'Prueba variables Globales a BC';
                trigger OnAction()
                var
                    clMensaje: Label 'Es valor actual es %1. Lo cambimaos a %2';
                begin
                    Message(clMensaje, culPSRFuncionesAppC01.GetSwitchF(), not culPSRFuncionesAppC01.GetSwitchF());
                    culPSRFuncionesAppC01.SetSwitchF(not culPSRFuncionesAppC01.GetSwitchF());

                end;
            }
            action(PSRPruebaCuCapturarErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Cu Capturar Errores';

                trigger OnAction()
                var
                    culPSRCapturaErroresC01: Codeunit PSRCapturaErroresC01;
                begin

                    if culPSRCapturaErroresC01.Run() then begin

                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end else begin
                        Message('No se ha podido ejecutar la Cu. Motivo (%1)', GetLastErrorText());
                    end;
                end;
            }
            action(PSRPrueba02CuCapturarErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Función Capturar Errores';

                trigger OnAction()
                var
                    culPSRCapturaErroresC01: Codeunit PSRCapturaErroresC01;
                    xlcodProveedor: Code[20];
                    i: Integer;
                begin
                    xlcodProveedor := '10000';
                    for i := 1 to 5 do begin

                        if culPSRCapturaErroresC01.FuncionParaCapturarErrorF(xlcodProveedor) then begin

                            Message('Se ha creado el proveedor %1', xlcodProveedor);
                        end;
                        xlcodProveedor := IncStr(xlcodProveedor);
                    end;
                    
                end;
            }

        }
    }
    var
        culPSRFuncionesAppC01: Codeunit "PSRVblesGlobalesAppC01";

}
