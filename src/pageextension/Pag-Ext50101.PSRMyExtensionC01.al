pageextension 50101 "PSRMyExtensionC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(PSREtiquetasC01)
            {
                ApplicationArea=all;
                trigger OnAction()
                var
                replEtiquetas : Report PSREtiquetaC01;
                rlSalesShipmentLines: Record "Sales Shipment Line";
                begin
                    rlSalesShipmentLines.SetRange("Document No.",Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLines);
                    replEtiquetas.runModal();
                    // Report.Run(50104,true,true,rlSalesShipmentLines);
                end;
            }
        }
         
        
    }
}