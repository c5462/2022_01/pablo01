page 50105 "PSRListaCabPlanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Cab. Plan Vacunacion';
    PageType = List;
    SourceTable = PSRCabPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = PSRFichaplanVacunacionC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;



    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(CodigoCabecera; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaIniciovacunacionPlanificada; Rec.FechaIniciovacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(Empresavacunadora; Rec.Empresavacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa vacunadora field.';
                    ApplicationArea = All;
                }

                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    ApplicationArea = All;
                    Caption = 'Nombre empresa vacunadora';
                    ToolTip = 'Aparecerá elnombre del proveedor que realizará la vacunación';
                }

            }
        }
    }
}
