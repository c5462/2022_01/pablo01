page 50101 "PSRListadevariosC01"
{
    ApplicationArea = All;
    Caption = 'Lista de tabla Varios';
    PageType = List;
    SourceTable = PSRVariosC01;
    UsageCategory = Lists;
    Editable = false;
    AdditionalSearchTerms = 'Curso 01';
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    CardPageId = PSRFichadeVariosC01;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(PSRCodigoC01; Rec.PSRCodigoC01)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field("PSRDescripciónC01"; Rec."PSRDescripciónC01")
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(PeriodoSegundaVacunacion; Rec.PeriodoSegundaVacunacion)
                {
                    ToolTip = 'Specifies the value of the PeriodoSegundaVacunacion field.';
                    ApplicationArea = All;
                }
                field(PSRTipoC01; Rec.PSRTipoC01)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the MyField field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
