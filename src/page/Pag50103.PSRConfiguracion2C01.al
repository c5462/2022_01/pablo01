page 50103 "PSRConfiguracion2C01"
{
    ApplicationArea = All;
    Caption = 'Lista conf. curso 01';
    PageType = List;
    SourceTable = PSRConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter=true;
    
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Unidades disponibles"; Rec."Unidades disponibles")
                {
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the ColorLetra field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Nos muestra el cliente web.';
                    ApplicationArea = All;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo Documento field.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de Tabla field.';
                    ApplicationArea = All;
                }
            }
        } 
    }
}
