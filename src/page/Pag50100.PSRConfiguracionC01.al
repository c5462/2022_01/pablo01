page 50100 "PSRConfiguracionC01"
{
    Caption = 'Configuración de la app 01 del curso ';
    PageType = Card;
    SourceTable = PSRConfiguracionC01;
    AboutTitle = 'En esta pagina crearemos la configuración de la primera app del curso';
    AboutText = 'Configure la app de forma correcta. Asegurese que asigan el valor del campo del cliente web';
    AdditionalSearchTerms = 'Mi primera app';
    ApplicationArea = All;
    UsageCategory = Administration;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter=true;

    layout
    {

        area(content)
        {



            group(General)
            {

                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo code. cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    Importance = Promoted;

                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                    AboutText = 'djsnvisdjttnvi';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo Documento field.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de Tabla field.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; Rec.nombreTablaF(Rec.TipoTabla, Rec.CodTabla))
                {
                    ApplicationArea = All;
                    Editable = false;
                    Caption = 'Nombre Tabla';
                }

                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the ColorLetra field.';
                    ApplicationArea = All;
                }

            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(Camposcalculados)
            {
                field(Nombrecliente; Rec.NombreCliente)
                {
                    ToolTip = 'Nos muestra el cliente web.';
                    ApplicationArea = All;
                }
                field(NombreClientePorFuncion; Rec.nombreclienteF(Rec.CodCteWeb))
                {
                    ApplicationArea = All;
                    Caption = 'Nombre cliente';
                }
                field(NumUnidadesVendidas; Rec."Unidades disponibles")
                {
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }


            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'Mensaje de acción';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Acción pulsada');

                end;
            }
        }

    }

    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;
}
