page 50106 "PSRSubPagLinPlanVacunacionC01"
{
    Caption = 'SubPagLinPlanVacunacionC01';
    PageType = ListPart;
    SourceTable = PSRLinPlanVacunacionC01;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(CodigoLinea; Rec.CodigoLinea)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                    Editable=false;
                    Visible=false;
                }
                field(NumLinea; Rec.NumLinea)
                {
                    ToolTip = 'Specifies the value of the Numero Linea field.';
                    ApplicationArea = All;
                    Editable=false;
                    Visible=false;


                }
                field("Codigo Cliente a Vacunar"; Rec."Codigo Cliente a Vacunar")
                {
                    ToolTip = 'Specifies the value of the CodCteVacunar field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreClienteF())
                {
                    ToolTip = 'Specifies the value of the FechaVacuna field.';
                    ApplicationArea = All;

                }
                field(FechaVacuna; Rec.FechaVacuna)
                {
                    ToolTip = 'Specifies the value of the FechaVacuna field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the TipoVacuna field.';
                    ApplicationArea = All;
                }
                field(DescVacuna; Rec.DescripcionVariosF(ePSRTipoC01::Vacuna, rec.CodigoVacuna))
                {
                    Caption = 'descripcion';
                    ApplicationArea = All;
                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                }
                field(DescOtros; Rec.DescripcionVariosF(ePSRTipoC01::Otros, rec.CodigoOtros))
                {
                    Caption = 'descripcion';
                    ApplicationArea = All;
                }
                field(FechaProximaVacunacion; Rec.FechaProximaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Proxima Vacuna field.';
                    ApplicationArea = All;
                }

            }
        }
    }
    var
        ePSRTipoC01: Enum PSRTipoC01;


// trigger OnNewRecord(BelowxRec: Boolean)
// var
// rlPSRCabPlanVacunacionC01: Record PSRCabPlanVacunacionC01;
// begin
//     if rlPSRCabPlanVacunacionC01.Get(Rec.CodigoLinea) then begin
//         Rec.Validate(FechaVacuna,rlPSRCabPlanVacunacionC01.FechaIniciovacunacionPlanificada);
//     end;
// end;



}
