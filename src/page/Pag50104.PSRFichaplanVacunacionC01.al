page 50104 "PSRFichaplanVacunacionC01"
{
    Caption = 'FichaplanVacunacionC01';
    PageType = Document;
    SourceTable = PSRCabPlanVacunacionC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(Empresavacunadora; Rec.Empresavacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaIniciovacunacionPlanificada; Rec.FechaIniciovacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre Empresa';
                    ApplicationArea = All;
                    ToolTip = 'Aparecerá elnombre del proveedor que realizará la vacunación';

                }


            }

            part(Lineas; PSRSubPagLinPlanVacunacionC01)
            {
                ApplicationArea = all;
                SubPageLink = CodigoLinea = field(CodigoCabecera);
            }
        }

    }

    actions
    {
        area(Processing)
        {
            action(EjemploVariables01)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables 01';

                trigger OnAction()
                var
                    culPSRFuncionesAppC01: Codeunit PSRFuncionesAppC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Pablo Sarrión';
                    culPSRFuncionesAppC01.EjemploVariablesESF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariables02)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables objetos 01';

                trigger OnAction()
                var
                    culPSRFuncionesAppC01: Codeunit PSRFuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Pablo Sarrión');
                    culPSRFuncionesAppC01.EjemploVariablesESF02(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(PruebaFcSegPlano)
            {
                ApplicationArea = All;
                Caption = 'Prueba Funcion Segundo Plano';

                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    // codeunit.Run(Codeunit::PSRCodeUnitLentaC01);
                    StartSession(xlSesionIniciada, Codeunit::PSRCodeUnitLentaC01);
                    Message('Proceso lanzado en segundo plano');

                end;
            }
            action(LogsPrimerPlano)
            {
                ApplicationArea = All;
                trigger OnAction()
                var
                    culPSRCodeUnitLentaC01: Codeunit PSRCodeUnitLentaC01;
                begin
                    culPSRCodeUnitLentaC01.Run();
                end;
            }

            action(Log)
            {
                ApplicationArea = All;
                Caption = 'Logs';
                Image = Log;
                RunObject = page PSRLogsC01;


            }

        }
    }
}
