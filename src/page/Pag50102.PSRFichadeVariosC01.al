page 50102 "PSRFichadeVariosC01"
{
    Caption = 'Ficha Registros Varios';
    PageType = Card;
    SourceTable = PSRVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar la tabla de registros';


    layout
    {
        area(content)
        {
            group(General)
            {
                field(PSRCodigoC01; Rec.PSRCodigoC01)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field("PSRDescripciónC01"; Rec."PSRDescripciónC01")
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(PeriodoSegundaVacunacion; Rec.PeriodoSegundaVacunacion)
                {
                    ToolTip = 'Specifies the value of the PeriodoSegundaVacunacion field.';
                    ApplicationArea = All;
                }
                field(PSRTipoC01; Rec.PSRTipoC01)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de Registro';
                    AboutText = 'Indicará tipo de registros y otros';
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the MyField field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
