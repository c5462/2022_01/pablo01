page 50107 "PSRLogsC01"
{
    ApplicationArea = All;
    Caption = 'LogsC01';
    PageType = List;
    SourceTable = PSRLogC01;
    UsageCategory = Lists;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    SourceTableView =sorting(SystemCreatedAt) order(descending);

    
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
            }
        }
    }

}
