reportextension 50100 "PSRNuevaExtC01" extends "Sales - Shipment"
{
    dataset
    {
        addlast("Sales Shipment Header")
        {
            dataitem("Sales Shipment Header1"; "Sales Shipment Header")
            {
                

                column(PSRShiptoName_SalesShipmentHeader1; "Ship-to Name")
                {
                }
                column(PSRShiptoAddress2_SalesShipmentHeader1; "Ship-to Address 2")
                {
                }
                column(PSRShiptoCity_SalesShipmentHeader1; "Ship-to City")
                {
                }
            }
        }

    }
}