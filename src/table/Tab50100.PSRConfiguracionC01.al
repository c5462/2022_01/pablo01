table 50100 "PSRConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;
    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. de regitro';
            DataClassification = SystemMetadata;
        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Cod. cliente para web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            ValidateTableRelation = true;


        }
        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; enum PSRenumC01)
        {
            Caption = 'Tipo Documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre cliente';
            fieldclass = flowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;

        }
        field(6; TipoTabla; Enum PSRTipoTablaC01)
        {
            Caption = 'Tipo de Tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if xrec.TipoTabla <> rec.TipoTabla then begin
                    Rec.Validate(CodTabla, '');
                end;

            end;
        }
        field(7; CodTabla; Code[20])
        {
            DataClassification = SystemMetadata;
            Caption = 'CodTabla';
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;
        }
        field(8; ColorFondo; Enum PSRColoresC01)
        {
            Caption = 'Color Fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum PSRColoresC01)
        {
            Caption = 'Color Letra';
            DataClassification = SystemMetadata;

        }
        field(10; "Unidades disponibles"; Decimal)
        {
            Caption = 'Unidades Disponibles';
            FieldClass = FlowField;
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto),"Posting Date"=field(FiltroFecha),"Entry Type"=field(FiltroTipoMovimiento)));
        }
        field(11; FiltroProducto; Code[20])
        {
            TableRelation = Item."No.";
            Caption = 'Filtro producto';
            FieldClass = FlowFilter;
        }
        field(12; FiltroFecha; Date)
        {
            Caption = 'Filtro fecha';
            FieldClass = FlowFilter;
        }
        field(13; FiltroTipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro tipo movimiento';
            FieldClass = FlowFilter;

        }

        




    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Id, CodCteWeb) { }
        fieldgroup(Brick; Id, CodCteWeb) { }
    }

    /// <summary>
    /// Recupera la documtación d ela tabala actual y sino lo crea
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not rec.Get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);

    end;
    /// <summary>
    /// Devuelve el nombre del cliente basado en el paramero pcodCliente, si no existe devolverá un exto vacio
    /// </summary>
    /// <param name="pCodCte"></param>
    /// <returns></returns>
    procedure nombreclienteF(pCodCte: Code[20]): Text
    var

        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(pCodCte) then begin
            exit(rlCustomer.Name);
        end;

    end;

    procedure nombreTablaF(pTipoTabla: Enum PSRTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlPSRConfiguracionC01: Record PSRConfiguracionC01;
        rlcontact: Record Contact;
        rlEmployee: Record Employee;
        rlvendor: Record Vendor;
        rlresource: Record Resource;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlPSRConfiguracionC01.nombreclienteF(pCodTabla));

                end;
            pTipoTabla::Contacto:
                begin
                    if rlcontact.Get(pCodTabla) then begin
                        exit(rlcontact.Name)
                    end;
                end;

            pTipoTabla::Empleado:
                begin
                    if rlEmployee.Get(pCodTabla) then begin
                        exit(rlEmployee.Name)
                    end;
                end;

            pTipoTabla::Proveedor:
                begin
                    if rlvendor.Get(pCodTabla) then begin
                        exit(rlvendor.Name)
                    end;
                end;

            pTipoTabla::Recurso:
                begin
                    if rlresource.Get(pCodTabla) then begin
                        exit(rlresource.Name)
                    end;
                end;


        end;

    end;
}

