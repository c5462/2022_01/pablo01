table 50102 "PSRCabPlanVacunacionC01"
{
    Caption = 'Cab. Plan VacunacionC01';
    DataClassification = ToBeClassified;
    
    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; FechaIniciovacunacionPlanificada; Date)
        {
            Caption = 'Fecha Inicio Vacunacion Planificada';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; Empresavacunadora; Code[20])
        {
            Caption = 'Empresa vacunadora';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation=Vendor."No.";
        }
        
    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }

    procedure NombreEmpresaVacunadoraF(): Text
    var
    rlVendor: Record Vendor;

    begin
        
        if rlVendor.Get(Rec.Empresavacunadora) then begin
            exit(rlVendor.Name);
        end;

    end;
}
