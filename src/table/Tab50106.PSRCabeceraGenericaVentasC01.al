table 50106 "PSRCabeceraGenericaVentasC01"
{
    Permissions = TableData "PSRLinGenericaVentasC01" = rimd;
Caption = 'Cab. ventas genérica';
    DataClassification = CustomerContent;
    fields
    {
        field(1; "Document Type"; Option)
        {
            Caption = 'Tipo documento';
            //OptionCaption = 'Oferta,Pedido,Factura,Abono,Pedido abierto,Devolución,Albarán,Recep. dev.';
            //OptionMembers = Quote,Order,Invoice,"Credit Memo","Blanket Order","Return Order",Shipment,"Return Rcpt.";
            OptionMembers = Oferta,Pedido,Factura,Abono,"Pedido abierto","Devolución","Albarán","Recep. dev.";
            DataClassification = CustomerContent;
        }
        field(2; "Sell-to Customer No."; Code[20])
        {
            Caption = 'Venta a-Nº cliente';
            DataClassification = CustomerContent;
            TableRelation = Customer;
        }
        field(3; "No."; Code[20])
        {
            Caption = 'No.';
            DataClassification = CustomerContent;
        }
        field(4; "Bill-to Customer No."; Code[20])
        {
            Caption = 'Factura-a Nº cliente';
            DataClassification = CustomerContent;
            NotBlank = true;
            TableRelation = Customer;
        }
        field(5; "Bill-to Name"; Text[100])
        {
            Caption = 'Fact. a-Nombre';
            DataClassification = CustomerContent;
            TableRelation = Customer;
            ValidateTableRelation = false;
        }
        field(6; "Bill-to Name 2"; Text[50])
        {
            Caption = 'Fact. a-Nombre 2';
            DataClassification = CustomerContent;
        }
        field(7; "Bill-to Address"; Text[100])
        {
            Caption = 'Fact. a-Dirección';
            DataClassification = CustomerContent;
        }
        field(8; "Bill-to Address 2"; Text[50])
        {
            Caption = 'Fact. a-Dirección 2';
            DataClassification = CustomerContent;
        }
        field(9; "Bill-to City"; Text[30])
        {
            Caption = 'Fact. a-Población';
            DataClassification = CustomerContent;
            TableRelation = IF ("Bill-to Country/Region Code" = CONST('')) "Post Code".City
            ELSE
            IF ("Bill-to Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Bill-to Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(10; "Bill-to Contact"; Text[100])
        {
            Caption = 'Fact. a-Atención';
            DataClassification = CustomerContent;
        }
        field(11; "Your Reference"; Text[35])
        {
            Caption = 'Su/Ntra. ref.';
            DataClassification = CustomerContent;
        }
        field(12; "Ship-to Code"; Code[10])
        {
            Caption = 'Cód. dirección envío cliente';
            DataClassification = CustomerContent;
            TableRelation = "Ship-to Address".Code WHERE("Customer No." = FIELD("Sell-to Customer No."));
        }
        field(13; "Ship-to Name"; Text[100])
        {
            Caption = 'Nombre dirección de envío';
            DataClassification = CustomerContent;
        }
        field(14; "Ship-to Name 2"; Text[50])
        {
            Caption = 'Nombre dirección de envío 2';
            DataClassification = CustomerContent;
        }
        field(15; "Ship-to Address"; Text[100])
        {
            Caption = 'Dirección de envío';
            DataClassification = CustomerContent;
        }
        field(16; "Ship-to Address 2"; Text[50])
        {
            Caption = 'Dirección de envío 2';
            DataClassification = CustomerContent;
        }
        field(17; "Ship-to City"; Text[30])
        {
            Caption = 'Población dirección de envío';
            DataClassification = CustomerContent;
            TableRelation = IF ("Ship-to Country/Region Code" = CONST('')) "Post Code".City
            ELSE
            IF ("Ship-to Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Ship-to Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(18; "Ship-to Contact"; Text[100])
        {
            Caption = 'Contacto de dirección de envío';
            DataClassification = CustomerContent;
        }
        field(19; "Order Date"; Date)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            Caption = 'Fecha pedido';
            DataClassification = CustomerContent;
        }
        field(20; "Posting Date"; Date)
        {
            Caption = 'Fecha registro';
            DataClassification = CustomerContent;
        }
        field(21; "Shipment Date"; Date)
        {
            Caption = 'Fecha envío';
            DataClassification = CustomerContent;
        }
        field(22; "Posting Description"; Text[100])
        {
            Caption = 'Texto de registro';
            DataClassification = CustomerContent;
        }
        field(23; "Payment Terms Code"; Code[10])
        {
            Caption = 'Cód. términos pago';
            DataClassification = CustomerContent;
            TableRelation = "Payment Terms";
        }
        field(24; "Due Date"; Date)
        {
            Caption = 'Fecha vencimiento';
            DataClassification = CustomerContent;
        }
        field(25; "Payment Discount %"; Decimal)
        {
            Caption = '% Dto. P.P.';
            DataClassification = CustomerContent;
            DecimalPlaces = 0 : 5;
            MaxValue = 100;
            MinValue = 0;
        }
        field(26; "Pmt. Discount Date"; Date)
        {
            Caption = 'Fecha dto. P.P.';
            DataClassification = CustomerContent;
        }
        field(27; "Shipment Method Code"; Code[10])
        {
            Caption = 'Cód. condiciones envío';
            DataClassification = CustomerContent;
            TableRelation = "Shipment Method";
        }
        field(28; "Location Code"; Code[10])
        {
            Caption = 'Cód. almacén';
            DataClassification = CustomerContent;
            TableRelation = Location WHERE("Use As In-Transit" = CONST(false));
        }
        field(29; "Shortcut Dimension 1 Code"; Code[20])
        {
            CaptionClass = '1,2,1';
            Caption = 'Cód. dim. acceso dir. 1';
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(1),
                                                          Blocked = CONST(false));
        }
        field(30; "Shortcut Dimension 2 Code"; Code[20])
        {
            CaptionClass = '1,2,2';
            Caption = 'Cód. dim. acceso dir. 2';
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(2),
                                                          Blocked = CONST(false));
        }
        field(31; "Customer Posting Group"; Code[20])
        {
            Caption = 'Grupo contable cliente';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Customer Posting Group";
        }
        field(32; "Currency Code"; Code[10])
        {
            Caption = 'Cód. divisa';
            DataClassification = CustomerContent;
            TableRelation = Currency;
        }
        field(33; "Currency Factor"; Decimal)
        {
            Caption = 'Factor divisa';
            DataClassification = CustomerContent;
            DecimalPlaces = 0 : 15;
            Editable = false;
            MinValue = 0;
        }
        field(34; "Customer Price Group"; Code[20])
        {
            Caption = 'Grupo precio cliente';
            DataClassification = CustomerContent;
            TableRelation = "Customer Price Group";
        }
        field(35; "Prices Including VAT"; Boolean)
        {
            Caption = 'Precios IVA incluido';
            DataClassification = CustomerContent;
        }
        field(37; "Invoice Disc. Code"; Code[20])
        {
            AccessByPermission = TableData "Cust. Invoice Disc." = R;
            Caption = 'Cód. dto. factura';
            DataClassification = CustomerContent;
        }
        field(40; "Customer Disc. Group"; Code[20])
        {
            Caption = 'Grupo dto. cliente';
            DataClassification = CustomerContent;
            TableRelation = "Customer Discount Group";
        }
        field(41; "Language Code"; Code[10])
        {
            Caption = 'Cód. idioma';
            DataClassification = CustomerContent;
            TableRelation = Language;
        }
        field(43; "Salesperson Code"; Code[20])
        {
            Caption = 'Cód. vendedor';
            DataClassification = CustomerContent;
            TableRelation = "Salesperson/Purchaser";
        }
        field(44; "Order No."; Code[20])
        {
            Caption = 'Nº pedido';
            DataClassification = CustomerContent;
        }
        field(45; "Order Class"; Code[10])
        {
            Caption = 'Clase pedido';
            DataClassification = CustomerContent;
        }
        field(46; Comment; Boolean)
        {
            CalcFormula = Exist("Sales Comment Line" WHERE("Document Type" = FIELD("Document Type"),
                                                            "No." = FIELD("No."),
                                                            "Document Line No." = CONST(0)));
            Caption = 'Comentario';
            Editable = false;
            FieldClass = FlowField;
        }
        field(47; "No. Printed"; Integer)
        {
            Caption = 'Nº copias impresas';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(51; "On Hold"; Code[3])
        {
            Caption = 'Esperar';
            DataClassification = CustomerContent;
        }
        field(52; "Applies-to Doc. Type"; Option)
        {
            Caption = 'Liq. por tipo documento';
            DataClassification = CustomerContent;
            OptionCaption = ' ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,,,,,,,,,,,,,,Bill';
            OptionMembers = " ",Payment,Invoice,"Credit Memo","Finance Charge Memo",Reminder,Refund,,,,,,,,,,,,,,,Bill;
        }
        field(53; "Applies-to Doc. No."; Code[20])
        {
            Caption = 'Liq. por nº documento';
            DataClassification = CustomerContent;
        }
        field(55; "Bal. Account No."; Code[20])
        {
            Caption = 'Cta. contrapartida';
            DataClassification = CustomerContent;
            TableRelation = IF ("Bal. Account Type" = CONST("G/L Account")) "G/L Account"
            ELSE
            IF ("Bal. Account Type" = CONST("Bank Account")) "Bank Account";
        }
        field(56; "Recalculate Invoice Disc."; Boolean)
        {
            CalcFormula = Exist("Sales Line" WHERE("Document Type" = FIELD("Document Type"),
                                                    "Document No." = FIELD("No."),
                                                    "Recalculate Invoice Disc." = CONST(true)));
            Caption = 'Recalcular dto. en la factura';
            Editable = false;
            FieldClass = FlowField;
        }
        field(57; Ship; Boolean)
        {
            Caption = 'Envío';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(58; Invoice; Boolean)
        {
            Caption = 'Factura';
            DataClassification = CustomerContent;
        }
        field(59; "Print Posted Documents"; Boolean)
        {
            Caption = 'Imprimir documentos registrados';
            DataClassification = CustomerContent;
        }
        field(60; Amount; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = Sum("PSRLinGenericaVentasC01".Amount WHERE("Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.")));
            Caption = 'Importe';
            Editable = false;
            FieldClass = FlowField;
        }
        field(61; "Amount Including VAT"; Decimal)
        {
            AutoFormatType = 1;
            Caption = 'Importe IVA incl.';
            Editable = false;
            FieldClass = FlowField;
        }
        field(62; "Shipping No."; Code[20])
        {
            Caption = 'Nº sig. albarán venta';
            DataClassification = CustomerContent;
        }
        field(63; "Posting No."; Code[20])
        {
            Caption = 'Nº sig. factura';
            DataClassification = CustomerContent;
        }
        field(64; "Last Shipping No."; Code[20])
        {
            Caption = 'Últ. nº albarán venta';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Sales Shipment Header";
        }
        field(65; "Last Posting No."; Code[20])
        {
            Caption = 'Últ. nº factura';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Sales Invoice Header";
        }
        field(66; "Prepayment No."; Code[20])
        {
            Caption = 'Nº prepago';
            DataClassification = CustomerContent;
        }
        field(67; "Last Prepayment No."; Code[20])
        {
            Caption = 'Nº último prepago';
            DataClassification = CustomerContent;
            TableRelation = "Sales Invoice Header";
        }
        field(68; "Prepmt. Cr. Memo No."; Code[20])
        {
            Caption = 'Nº abono prepago';
            DataClassification = CustomerContent;
        }
        field(69; "Last Prepmt. Cr. Memo No."; Code[20])
        {
            Caption = 'Nº último abono prepago';
            DataClassification = CustomerContent;
            TableRelation = "Sales Cr.Memo Header";
        }
        field(70; "VAT Registration No."; Text[20])
        {
            Caption = 'CIF/NIF';
            DataClassification = CustomerContent;
        }
        field(71; "Combine Shipments"; Boolean)
        {
            Caption = 'Fact. automática';
            DataClassification = CustomerContent;
        }
        field(73; "Reason Code"; Code[10])
        {
            Caption = 'Cód. auditoría';
            DataClassification = CustomerContent;
            TableRelation = "Reason Code";
        }
        field(74; "Gen. Bus. Posting Group"; Code[20])
        {
            Caption = 'Grupo contable negocio';
            DataClassification = CustomerContent;
            TableRelation = "Gen. Business Posting Group";
        }
        field(75; "EU 3-Party Trade"; Boolean)
        {
            Caption = 'Op. triangular';
            DataClassification = CustomerContent;
        }
        field(76; "Transaction Type"; Code[10])
        {
            Caption = 'Naturaleza transacción';
            DataClassification = CustomerContent;
            TableRelation = "Transaction Type";
        }
        field(77; "Transport Method"; Code[10])
        {
            Caption = 'Modo transporte';
            DataClassification = CustomerContent;
            TableRelation = "Transport Method";
        }
        field(78; "VAT Country/Region Code"; Code[10])
        {
            Caption = 'Cód. IVA país/región';
            DataClassification = CustomerContent;
            TableRelation = "Country/Region";
        }
        field(79; "Sell-to Customer Name"; Text[100])
        {
            Caption = 'Venta a-Nombre';
            DataClassification = CustomerContent;
            TableRelation = Customer;
            ValidateTableRelation = false;
        }
        field(80; "Sell-to Customer Name 2"; Text[50])
        {
            Caption = 'Venta a-Nombre 2';
            DataClassification = CustomerContent;
        }
        field(81; "Sell-to Address"; Text[100])
        {
            Caption = 'Venta a-Dirección';
            DataClassification = CustomerContent;
        }
        field(82; "Sell-to Address 2"; Text[50])
        {
            Caption = 'Venta a-Dirección 2';
            DataClassification = CustomerContent;
        }
        field(83; "Sell-to City"; Text[30])
        {
            Caption = 'Venta a-Población';
            DataClassification = CustomerContent;
            TableRelation = IF ("Sell-to Country/Region Code" = CONST('')) "Post Code".City
            ELSE
            IF ("Sell-to Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Sell-to Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(84; "Sell-to Contact"; Text[100])
        {
            Caption = 'Venta a-Atención';
            DataClassification = CustomerContent;
        }
        field(85; "Bill-to Post Code"; Code[20])
        {
            Caption = 'Fact. a-C.P.';
            DataClassification = CustomerContent;
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(86; "Bill-to County"; Text[30])
        {
            Caption = 'Fact. a-Provincia';
            DataClassification = CustomerContent;
        }
        field(87; "Bill-to Country/Region Code"; Code[10])
        {
            Caption = 'Fact. a-Cód. país/región';
            DataClassification = CustomerContent;
            TableRelation = "Country/Region";
        }
        field(88; "Sell-to Post Code"; Code[20])
        {
            Caption = 'Venta a-C.P.';
            DataClassification = CustomerContent;
            TableRelation = IF ("Sell-to Country/Region Code" = CONST('')) "Post Code"
            ELSE
            IF ("Sell-to Country/Region Code" = FILTER(<> '')) "Post Code" WHERE("Country/Region Code" = FIELD("Sell-to Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(89; "Sell-to County"; Text[30])
        {
            Caption = 'Venta a-Provincia';
            DataClassification = CustomerContent;
        }
        field(90; "Sell-to Country/Region Code"; Code[10])
        {
            Caption = 'Venta a-Cód. país/región';
            DataClassification = CustomerContent;
            TableRelation = "Country/Region";
        }
        field(91; "Ship-to Post Code"; Code[20])
        {
            Caption = 'C.P. dirección de envío';
            DataClassification = CustomerContent;
            TableRelation = IF ("Ship-to Country/Region Code" = CONST('')) "Post Code"
            ELSE
            IF ("Ship-to Country/Region Code" = FILTER(<> '')) "Post Code" WHERE("Country/Region Code" = FIELD("Ship-to Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(92; "Ship-to County"; Text[30])
        {
            Caption = 'Provincia dirección de envío';
            DataClassification = CustomerContent;
        }
        field(93; "Ship-to Country/Region Code"; Code[10])
        {
            Caption = 'Cód. país/región dirección de envío';
            DataClassification = CustomerContent;
            TableRelation = "Country/Region";
        }
        field(94; "Bal. Account Type"; Option)
        {
            Caption = 'Tipo contrapartida';
            DataClassification = CustomerContent;
            OptionCaption = 'G/L Account,Bank Account';
            OptionMembers = "G/L Account","Bank Account";
        }
        field(97; "Exit Point"; Code[10])
        {
            Caption = 'Puerto/Aerop. carga';
            DataClassification = CustomerContent;
            TableRelation = "Entry/Exit Point";
        }
        field(98; Correction; Boolean)
        {
            Caption = 'Corrección';
            DataClassification = CustomerContent;
        }
        field(99; "Document Date"; Date)
        {
            Caption = 'Fecha emisión documento';
            DataClassification = CustomerContent;
        }
        field(100; "External Document No."; Code[35])
        {
            Caption = 'Nº documento externo';
            DataClassification = CustomerContent;
        }
        field(101; "Area"; Code[10])
        {
            Caption = 'Cód. provincia';
            DataClassification = CustomerContent;
            TableRelation = Area;
        }
        field(102; "Transaction Specification"; Code[10])
        {
            Caption = 'Especificación transacción';
            DataClassification = CustomerContent;
            TableRelation = "Transaction Specification";
        }
        field(104; "Payment Method Code"; Code[10])
        {
            Caption = 'Cód. forma pago';
            DataClassification = CustomerContent;
            TableRelation = "Payment Method";
        }
        field(105; "Shipping Agent Code"; Code[10])
        {
            AccessByPermission = TableData "Shipping Agent Services" = R;
            Caption = 'Cód. transportista';
            DataClassification = CustomerContent;
            TableRelation = "Shipping Agent";
        }
        field(106; "Package Tracking No."; Text[30])
        {
            Caption = 'Nº seguimiento bulto';
            DataClassification = CustomerContent;
        }
        field(107; "No. Series"; Code[20])
        {
            Caption = 'Nos. serie';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "No. Series";
        }
        field(108; "Posting No. Series"; Code[20])
        {
            Caption = 'Nº serie registro';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(109; "Shipping No. Series"; Code[20])
        {
            Caption = 'Nº serie alb. venta';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(110; "Order No. Series"; Code[20])
        {
            Caption = 'Nº serie pedido';
            TableRelation = "No. Series";
            DataClassification = CustomerContent;
        }
        field(111; "Pre-Assigned No."; Code[20])
        {
            Caption = 'Nº preasignado';
            DataClassification = CustomerContent;
        }
        field(112; "User ID"; Code[50])
        {
            Caption = 'Id. usuario';
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
        }
        field(113; "Source Code"; Code[10])
        {
            Caption = 'Cód. origen';
            TableRelation = "Source Code";
            DataClassification = CustomerContent;
        }
        field(114; "Tax Area Code"; Code[20])
        {
            Caption = 'Cód. área impuesto';
            DataClassification = CustomerContent;
            TableRelation = "Tax Area";
            ValidateTableRelation = false;
        }
        field(115; "Tax Liable"; Boolean)
        {
            Caption = 'Sujeto a impuesto';
            DataClassification = CustomerContent;
        }
        field(116; "VAT Bus. Posting Group"; Code[20])
        {
            Caption = 'Grupo registro IVA neg.';
            DataClassification = CustomerContent;
            TableRelation = "VAT Business Posting Group";
        }
        field(117; Reserve; Option)
        {
            AccessByPermission = TableData Item = R;
            Caption = 'Reserva';
            DataClassification = CustomerContent;
            InitValue = Optional;
            OptionCaption = 'Never,Optional,Always';
            OptionMembers = Never,Optional,Always;
        }
        field(118; "Applies-to ID"; Code[50])
        {
            Caption = 'Liq. por id.';
            DataClassification = CustomerContent;
        }
        field(119; "VAT Base Discount %"; Decimal)
        {
            Caption = '% Dto. base IVA';
            DataClassification = CustomerContent;
            DecimalPlaces = 0 : 5;
            MaxValue = 100;
            MinValue = 0;
        }
        field(120; Status; Option)
        {
            Caption = 'Estado';
            DataClassification = CustomerContent;
            Editable = false;
            OptionCaption = 'Open,Released,Pending Approval,Pending Prepayment';
            OptionMembers = Open,Released,"Pending Approval","Pending Prepayment";
        }
        field(121; "Invoice Discount Calculation"; Option)
        {
            Caption = 'Cálculo descuento factura';
            DataClassification = CustomerContent;
            Editable = false;
            OptionCaption = 'None,%,Amount';
            OptionMembers = "None","%",Amount;
        }
        field(122; "Invoice Discount Value"; Decimal)
        {
            AutoFormatType = 1;
            Caption = 'Valor descuento factura';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(123; "Send IC Document"; Boolean)
        {
            Caption = 'Enviar documento IC';
            DataClassification = CustomerContent;
        }
        field(124; "IC Status"; Option)
        {
            Caption = 'Estado socio IC';
            DataClassification = CustomerContent;
            OptionCaption = 'New,Pending,Sent';
            OptionMembers = New,Pending,Sent;
        }
        field(125; "Sell-to IC Partner Code"; Code[20])
        {
            Caption = 'Código IC asociada de venta';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "IC Partner";
        }
        field(126; "Bill-to IC Partner Code"; Code[20])
        {
            Caption = 'Código IC asociada facturación';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "IC Partner";
        }
        field(129; "IC Direction"; Option)
        {
            Caption = 'Dirección envío IC';
            DataClassification = CustomerContent;
            OptionCaption = 'Outgoing,Incoming';
            OptionMembers = Outgoing,Incoming;
        }
        field(130; "Prepayment %"; Decimal)
        {
            Caption = '% prepago';
            DataClassification = CustomerContent;
            DecimalPlaces = 0 : 5;
            MaxValue = 100;
            MinValue = 0;
        }
        field(131; "Prepayment No. Series"; Code[20])
        {
            Caption = 'Nº serie prepago';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(132; "Compress Prepayment"; Boolean)
        {
            Caption = 'Compresión prepago';
            DataClassification = CustomerContent;
            InitValue = true;
        }
        field(133; "Prepayment Due Date"; Date)
        {
            Caption = 'Fecha vencimiento prepago';
            DataClassification = CustomerContent;
        }
        field(134; "Prepmt. Cr. Memo No. Series"; Code[20])
        {
            Caption = 'Nº serie abono prepago';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(135; "Prepmt. Posting Description"; Text[100])
        {
            Caption = 'Texto registro prepago';
            DataClassification = CustomerContent;
        }
        field(136; "Prepayment Invoice"; Boolean)
        {
            Caption = 'Factura prepago';
            DataClassification = CustomerContent;
        }
        field(137; "Prepayment Order No."; Code[20])
        {
            Caption = 'Nº pedido prepago';
            DataClassification = CustomerContent;
        }
        field(138; "Prepmt. Pmt. Discount Date"; Date)
        {
            Caption = 'Fecha descuento prepago';
            DataClassification = CustomerContent;
        }
        field(139; "Prepmt. Payment Terms Code"; Code[10])
        {
            Caption = 'Código términos prepago';
            DataClassification = CustomerContent;
            TableRelation = "Payment Terms";
        }
        field(140; "Prepmt. Payment Discount %"; Decimal)
        {
            Caption = '% descuento prepago';
            DataClassification = CustomerContent;
            DecimalPlaces = 0 : 5;
            MaxValue = 100;
            MinValue = 0;
        }
        field(151; "Quote No."; Code[20])
        {
            Caption = 'Nº oferta';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(152; "Quote Valid Until Date"; Date)
        {
            Caption = 'Oferta válida hasta fecha';
            DataClassification = CustomerContent;
        }
        field(153; "Quote Sent to Customer"; DateTime)
        {
            Caption = 'Oferta enviada al cliente';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(154; "Quote Accepted"; Boolean)
        {
            Caption = 'Oferta aceptada';
            DataClassification = CustomerContent;
        }
        field(155; "Quote Accepted Date"; Date)
        {
            Caption = 'Fecha de oferta aceptada';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(160; "Job Queue Status"; Option)
        {
            Caption = 'Estado de cola de proyectos';
            DataClassification = CustomerContent;
            Editable = false;
            OptionCaption = ' ,Scheduled for Posting,Error,Posting';
            OptionMembers = " ","Scheduled for Posting",Error,Posting;
        }
        field(161; "Job Queue Entry ID"; Guid)
        {
            Caption = 'Id. mov. cola proyecto';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(165; "Incoming Document Entry No."; Integer)
        {
            Caption = 'Nº mov. de documentos entrantes';
            DataClassification = CustomerContent;
            TableRelation = "Incoming Document";
        }
        field(166; "Last Email Sent Time"; DateTime)
        {
            CalcFormula = Max("O365 Document Sent History"."Created Date-Time" WHERE("Document Type" = FIELD("Document Type"),
                                                                                      "Document No." = FIELD("No."),
                                                                                      Posted = CONST(false)));
            Caption = 'Hora del último correo electrónico enviado';
            FieldClass = FlowField;
        }
        field(167; "Last Email Sent Status"; Option)
        {
            CalcFormula = Lookup("O365 Document Sent History"."Job Last Status" WHERE("Document Type" = FIELD("Document Type"),
                                                                                       "Document No." = FIELD("No."),
                                                                                       Posted = CONST(false),
                                                                                       "Created Date-Time" = FIELD("Last Email Sent Time")));
            Caption = 'Estado del último correo electrónico enviado';
            FieldClass = FlowField;
            OptionCaption = 'Not Sent,In Process,Finished,Error';
            OptionMembers = "Not Sent","In Process",Finished,Error;
        }
        field(168; "Sent as Email"; Boolean)
        {
            CalcFormula = Exist("O365 Document Sent History" WHERE("Document Type" = FIELD("Document Type"),
                                                                    "Document No." = FIELD("No."),
                                                                    Posted = CONST(false),
                                                                    "Job Last Status" = CONST(Finished)));
            Caption = 'Enviado como correo electrónico';
            FieldClass = FlowField;
        }
        field(169; "Last Email Notif Cleared"; Boolean)
        {
            CalcFormula = Lookup("O365 Document Sent History".NotificationCleared WHERE("Document Type" = FIELD("Document Type"),
                                                                                         "Document No." = FIELD("No."),
                                                                                         Posted = CONST(false),
                                                                                         "Created Date-Time" = FIELD("Last Email Sent Time")));
            Caption = 'Última notif. correo electrónico borrada';
            FieldClass = FlowField;
        }
        field(200; "Work Description"; BLOB)
        {
            Caption = 'Descripción del trabajo';
            DataClassification = CustomerContent;
        }
        field(300; "Amt. Ship. Not Inv. (LCY)"; Decimal)
        {
            CalcFormula = Sum("PSRLinGenericaVentasC01"."Shipped Not Invoiced (LCY)" WHERE("Document No." = FIELD("No.")));
            Caption = 'Importe enviado no facturado (DL) con IVA';
            Editable = false;
            FieldClass = FlowField;
        }
        field(301; "Amt. Ship. Not Inv. (LCY) Base"; Decimal)
        {
            CalcFormula = Sum("PSRLinGenericaVentasC01"."Shipped Not Inv. (LCY) No VAT" WHERE("Document No." = FIELD("No.")));
            Caption = 'Importe enviado no facturado (DL)';
            Editable = false;
            FieldClass = FlowField;
        }
        field(480; "Dimension Set ID"; Integer)
        {
            Caption = 'Id. grupo dimensiones';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Dimension Set Entry";
        }
        field(600; "Payment Service Set ID"; Integer)
        {
            Caption = 'Id. de conjunto de servicios de pago';
            DataClassification = CustomerContent;
        }
        field(710; "Document Exchange Identifier"; Text[50])
        {
            Caption = 'Identificador de intercambio de documentos';
            DataClassification = CustomerContent;
        }
        field(711; "Document Exchange Status"; Option)
        {
            Caption = 'Estado de intercambio de documentos';
            DataClassification = CustomerContent;
            OptionCaption = 'Not Sent,Sent to Document Exchange Service,Delivered to Recipient,Delivery Failed,Pending Connection to Recipient';
            OptionMembers = "Not Sent","Sent to Document Exchange Service","Delivered to Recipient","Delivery Failed","Pending Connection to Recipient";
        }
        field(712; "Doc. Exch. Original Identifier"; Text[50])
        {
            Caption = 'Identificador original de intercambio documentos';
            DataClassification = CustomerContent;
        }
        field(720; "Coupled to CRM"; Boolean)
        {
            Caption = 'Emparejado con Dynamics 365 for Sales';
            DataClassification = CustomerContent;
        }
        field(1200; "Direct Debit Mandate ID"; Code[35])
        {
            Caption = 'Id. de orden de domiciliación de adeudo directo';
            DataClassification = CustomerContent;
            TableRelation = "SEPA Direct Debit Mandate" WHERE("Customer No." = FIELD("Bill-to Customer No."),
                                                               Closed = CONST(false),
                                                               Blocked = CONST(false));
        }
        field(1302; Closed; Boolean)
        {
            CalcFormula = - Exist("Cust. Ledger Entry" WHERE("Entry No." = FIELD("Cust. Ledger Entry No."),
                                                             Open = FILTER(true)));
            Caption = 'Cerrado';
            Editable = false;
            FieldClass = FlowField;
        }
        field(1303; "Remaining Amount"; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = Sum("Detailed Cust. Ledg. Entry".Amount WHERE("Cust. Ledger Entry No." = FIELD("Cust. Ledger Entry No.")));
            Caption = 'Importe pendiente';
            Editable = false;
            FieldClass = FlowField;
        }
        field(1304; "Cust. Ledger Entry No."; Integer)
        {
            Caption = 'Nº mov. cliente';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Cust. Ledger Entry"."Entry No.";
            //This property is currently not supported
            //TestTableRelation = false;
        }
        field(1305; "Invoice Discount Amount"; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = Sum("PSRLinGenericaVentasC01"."Inv. Discount Amount" WHERE("Document No." = FIELD("No."),
                                                                                  "Document Type" = FIELD("Document Type")));
            Caption = 'Importe descuento factura';
            Editable = false;
            FieldClass = FlowField;
        }
        field(1310; Cancelled; Boolean)
        {
            CalcFormula = Exist("Cancelled Document" WHERE("Source ID" = CONST(112),
                                                            "Cancelled Doc. No." = FIELD("No.")));
            Caption = 'Cancelado';
            Editable = false;
            FieldClass = FlowField;
        }
        field(1311; Corrective; Boolean)
        {
            CalcFormula = Exist("Cancelled Document" WHERE("Source ID" = CONST(114),
                                                            "Cancelled By Doc. No." = FIELD("No.")));
            Caption = 'Correctivo';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5043; "No. of Archived Versions"; Integer)
        {
            CalcFormula = Max("Sales Header Archive"."Version No." WHERE("Document Type" = FIELD("Document Type"),
                                                                          "No." = FIELD("No."),
                                                                          "Doc. No. Occurrence" = FIELD("Doc. No. Occurrence")));
            Caption = 'Nº de versiones archivadas';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5048; "Doc. No. Occurrence"; Integer)
        {
            Caption = 'Ocurrencia Nº doc.';
            DataClassification = CustomerContent;
        }
        field(5050; "Campaign No."; Code[20])
        {
            Caption = 'Nº campaña';
            DataClassification = CustomerContent;
            TableRelation = Campaign;
        }
        field(5051; "Sell-to Customer Template Code"; Code[10])
        {
            Caption = 'Venta a-Cód. plant. cliente';
            DataClassification = CustomerContent;
        }
        field(5052; "Sell-to Contact No."; Code[20])
        {
            Caption = 'Venta a-Nº contacto';
            DataClassification = CustomerContent;
            TableRelation = Contact;
        }
        field(5053; "Bill-to Contact No."; Code[20])
        {
            Caption = 'Fact. a-Nº contacto';
            DataClassification = CustomerContent;
            TableRelation = Contact;
        }
        field(5054; "Bill-to Customer Template Code"; Code[10])
        {
            Caption = 'Fact. a-Cód. plant. cliente';
            DataClassification = CustomerContent;
        }
        field(5055; "Opportunity No."; Code[20])
        {
            Caption = 'Nº oportunidad';
            DataClassification = CustomerContent;
        }
        field(5700; "Responsibility Center"; Code[10])
        {
            Caption = 'Centro responsabilidad';
            DataClassification = CustomerContent;
            TableRelation = "Responsibility Center";
        }
        field(5750; "Shipping Advice"; Option)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            Caption = 'Aviso envío';
            DataClassification = CustomerContent;
            OptionCaption = 'Partial,Complete';
            OptionMembers = Partial,Complete;
        }
        field(5751; "Shipped Not Invoiced"; Boolean)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            CalcFormula = Exist("Sales Line" WHERE("Document Type" = FIELD("Document Type"),
                                                    "Document No." = FIELD("No."),
                                                    "Qty. Shipped Not Invoiced" = FILTER(<> 0)));
            Caption = 'Enviado no facturado';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5752; "Completely Shipped"; Boolean)
        {
            CalcFormula = Min("PSRLinGenericaVentasC01"."Completely Shipped" WHERE("Document Type" = FIELD("Document Type"),
                                                                                "Document No." = FIELD("No."),
                                                                                Type = FILTER(<> " "),
                                                                                "Location Code" = FIELD("Location Filter")));
            Caption = 'Enviado completamente';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5753; "Posting from Whse. Ref."; Integer)
        {
            AccessByPermission = TableData Location = R;
            Caption = 'Regis. desde almac. ref.';
            DataClassification = CustomerContent;
        }
        field(5754; "Location Filter"; Code[10])
        {
            Caption = 'Filtro almacén';
            FieldClass = FlowFilter;
            TableRelation = Location;
        }
        field(5755; Shipped; Boolean)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            CalcFormula = Exist("PSRLinGenericaVentasC01" WHERE("Document Type" = FIELD("Document Type"),
                                                             "Document No." = FIELD("No."),
                                                             "Qty. Shipped (Base)" = FILTER(<> 0)));
            Caption = 'Enviado';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5790; "Requested Delivery Date"; Date)
        {
            AccessByPermission = TableData "Order Promising Line" = R;
            Caption = 'Fecha entrega requerida';
            DataClassification = CustomerContent;
        }
        field(5791; "Promised Delivery Date"; Date)
        {
            AccessByPermission = TableData "Order Promising Line" = R;
            Caption = 'Fecha entrega prometida';
            DataClassification = CustomerContent;
        }
        field(5792; "Shipping Time"; DateFormula)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            Caption = 'Tiempo envío';
            DataClassification = CustomerContent;
        }
        field(5793; "Outbound Whse. Handling Time"; DateFormula)
        {
            AccessByPermission = TableData "Warehouse Shipment Header" = R;
            Caption = 'Tiempo manip. alm. salida';
            DataClassification = CustomerContent;
        }
        field(5794; "Shipping Agent Service Code"; Code[10])
        {
            Caption = 'Cód. servicio transportista';
            DataClassification = CustomerContent;
            TableRelation = "Shipping Agent Services".Code WHERE("Shipping Agent Code" = FIELD("Shipping Agent Code"));
        }
        field(5795; "Late Order Shipping"; Boolean)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            CalcFormula = Exist("PSRLinGenericaVentasC01" WHERE("Document Type" = FIELD("Document Type"),
                                                             "Sell-to Customer No." = FIELD("Sell-to Customer No."),
                                                             "Document No." = FIELD("No."),
                                                             "Shipment Date" = FIELD("Date Filter"),
                                                             "Outstanding Quantity" = FILTER(<> 0)));
            Caption = 'Envío retrasado';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5796; "Date Filter"; Date)
        {
            Caption = 'Filtro fecha';
            FieldClass = FlowFilter;
        }
        field(5800; Receive; Boolean)
        {
            Caption = 'Recepción';
            DataClassification = CustomerContent;
        }
        field(5801; "Return Receipt No."; Code[20])
        {
            Caption = 'Nº recep. devol.';
            DataClassification = CustomerContent;
        }
        field(5802; "Return Receipt No. Series"; Code[20])
        {
            Caption = 'Nº serie recep. devol.';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(5803; "Last Return Receipt No."; Code[20])
        {
            Caption = 'Nº última recep. devol.';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Return Receipt Header";
        }
        field(6601; "Return Order No."; Code[20])
        {
            Caption = 'Nº devolución';
            DataClassification = CustomerContent;
        }
        field(6602; "Return Order No. Series"; Code[20])
        {
            Caption = 'Nº serie devolución';
            DataClassification = CustomerContent;
            TableRelation = "No. Series";
        }
        field(7001; "Allow Line Disc."; Boolean)
        {
            Caption = 'Permite dto. línea';
            DataClassification = CustomerContent;
        }
        field(7200; "Get Shipment Used"; Boolean)
        {
            Caption = 'Obtener método de envío usado';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(8000; Id; Guid)
        {
            Caption = 'Id';
            DataClassification = CustomerContent;
        }
        field(9000; "Assigned User ID"; Code[50])
        {
            Caption = 'Id. usuario asignado';
            DataClassification = CustomerContent;
            TableRelation = "User Setup";
        }
        field(10705; "Corrected Invoice No."; Code[20])
        {
            Caption = 'N.º factura corregida';
            DataClassification = CustomerContent;
        }
        field(10706; "Due Date Modified"; Boolean)
        {
            Caption = 'Fecha vencimiento modificada';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(10707; "Invoice Type"; Option)
        {
            Caption = 'Tipo de factura';
            DataClassification = CustomerContent;
            OptionCaption = 'F1 Invoice,F2 Simplified Invoice,F3 Invoice issued to replace simplified invoices,F4 Invoice summary entry';
            OptionMembers = "F1 Invoice","F2 Simplified Invoice","F3 Invoice issued to replace simplified invoices","F4 Invoice summary entry";
        }
        field(10708; "Cr. Memo Type"; Option)
        {
            Caption = 'Tipo de abono';
            DataClassification = CustomerContent;
            OptionCaption = 'R1 Corrected Invoice,R2 Corrected Invoice (Art. 80.3),R3 Corrected Invoice (Art. 80.4),R4 Corrected Invoice (Other),R5 Corrected Invoice in Simplified Invoices,F1 Invoice,F2 Simplified Invoice';
            OptionMembers = "R1 Corrected Invoice","R2 Corrected Invoice (Art. 80.3)","R3 Corrected Invoice (Art. 80.4)","R4 Corrected Invoice (Other)","R5 Corrected Invoice in Simplified Invoices","F1 Invoice","F2 Simplified Invoice";
        }
        field(10709; "Special Scheme Code"; Option)
        {
            Caption = 'Cód. de esquema especial';
            DataClassification = CustomerContent;
            OptionCaption = '01 General,02 Export,03 Special System,04 Gold,05 Travel Agencies,06 Groups of Entities,07 Special Cash,08  IPSI / IGIC,09 Travel Agency Services,10 Third Party,11 Business Withholding,12 Business not Withholding,13 Business Withholding and not Withholding,14 Invoice Work Certification,15 Invoice of Consecutive Nature,16 First Half 2017';
            OptionMembers = "01 General","02 Export","03 Special System","04 Gold","05 Travel Agencies","06 Groups of Entities","07 Special Cash","08  IPSI / IGIC","09 Travel Agency Services","10 Third Party","11 Business Withholding","12 Business not Withholding","13 Business Withholding and not Withholding","14 Invoice Work Certification","15 Invoice of Consecutive Nature","16 First Half 2017";
        }
        field(10710; "Operation Description"; Text[250])
        {
            Caption = 'Descripción de la operación';
            DataClassification = CustomerContent;
        }
        field(10711; "Correction Type"; Option)
        {
            Caption = 'Tipo de corrección';
            DataClassification = CustomerContent;
            OptionCaption = ' ,Replacement,Difference,Removal';
            OptionMembers = " ",Replacement,Difference,Removal;
        }
        field(10712; "Operation Description 2"; Text[250])
        {
            Caption = 'Descripción de la operación 2';
            DataClassification = CustomerContent;
        }
        field(7000000; "Applies-to Bill No."; Code[20])
        {
            Caption = 'Liq. por nº efecto';
            DataClassification = CustomerContent;
        }
        field(7000001; "Cust. Bank Acc. Code"; Code[20])
        {
            Caption = 'Cód. banco cliente';
            DataClassification = CustomerContent;
            TableRelation = "Customer Bank Account".Code WHERE("Customer No." = FIELD("Bill-to Customer No."));
        }
        field(7000003; "Pay-at Code"; Code[10])
        {
            Caption = 'Pago en-Código';
            DataClassification = CustomerContent;
            TableRelation = "Customer Pmt. Address".Code WHERE("Customer No." = FIELD("Bill-to Customer No."));
        }
        field(7042930; RecordIdOrigen; RecordId)
        {
            DataClassification = CustomerContent;
        }
        field(7043000; "Code retention"; Code[20])
        {
            DataClassification = CustomerContent;
            ObsoleteState = Removed;
            ObsoleteReason = 'Se pasa a una extensión en su APP';
        }
        field(7043001; "% Retention"; Decimal)
        {
            DataClassification = CustomerContent;
            ObsoleteState = Removed;
            ObsoleteReason = 'Se pasa a una extensión en su APP';
        }
        field(7043014; "Tipo documento impresion"; Option)
        {
            DataClassification = CustomerContent;
            OptionMembers = "Factura venta","Abono venta","Factura servicio","Abono servicio",Proforma;

            trigger OnValidate()
            var
                clAbono: Label 'Abono';
                clFactura: Label 'Factura';
                clFactProforma: Label 'Factura proforma';
            begin
                // *** Segun el tipo de documento pongo un titulo al documento.
                case "Tipo documento impresion" of
                    Rec."Tipo documento impresion"::"Factura venta":
                        "Titulo documento" := clFactura;
                    Rec."Tipo documento impresion"::"Abono venta":
                        "Titulo documento" := clAbono;
                    Rec."Tipo documento impresion"::"Factura servicio":
                        "Titulo documento" := clFactura;
                    Rec."Tipo documento impresion"::"Abono servicio":
                        "Titulo documento" := clAbono;
                    Rec."Tipo documento impresion"::Proforma:
                        "Titulo documento" := clFactProforma;
                end;
            end;
        }
        field(7043015; "Titulo documento"; Text[30])
        {
            DataClassification = CustomerContent;
        }
        field(7077251; "Id Sesion"; Integer)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Document Type", "No.", "Id Sesion")
        {
        }
    }

    fieldgroups
    {
    }

    trigger OnDelete()
    var
        rlLinGenericaVentas: Record "PSRLinGenericaVentasC01";
    begin
        rlLinGenericaVentas.SetRange("Id Sesion", "Id Sesion");
        rlLinGenericaVentas.DeleteAll(true);
    end;

    var
        rCustLedgerEntry: Record "Cust. Ledger Entry";
        rCustLedgerEntry2: Record "Cust. Ledger Entry";
        xDtoFra: Decimal;
        xTotalLiquidacion: Decimal;
        int: Integer;
        xDimBaseIVARE: Integer;
        xDimDtoFra: Integer;
        xDimDtoPP: Integer;
        xDimFechaVto: Integer;
        xDimImpLinea: Integer;
        xDimImporteIVA: Integer;
        xDimImporteRE: Integer;
        xDimImporteVto: Integer;
        // vDatosVencimiento: array[50, 4] of Text[100];
        xDimPorIVA: Integer;
        xDimPorRE: Integer;
        xNMovDocu: Integer;
        xLiquidaciones: array[2, 4] of Text;

    /// <summary>
    /// CalcVATAmountLinesF.
    /// </summary>
    /// <param name="plDatosImportesPie">VAR array[4, 8] of Decimal.</param>
    /// <param name="plTotalesImportesPie">VAR array[1, 8] of Decimal.</param>
    /// <param name="plTotalFra">VAR Decimal.</param>
    /// <param name="prTCNLinGenericaVentas">VAR Record PSRLinGenericaVentasC01.</param>
    procedure CalcVATAmountLinesF(var plDatosImportesPie: array[4, 8] of Decimal; var plTotalesImportesPie: array[1, 8] of Decimal; var plTotalFra: Decimal; var prTCNLinGenericaVentas: Record PSRLinGenericaVentasC01)
    var
        TemplVATAmountLine: Record "VAT Amount Line" temporary;
        i: Integer;
        cImpresionFormatoIva: Label 'Avise a desarrollo. NO se permite la impresión de más de 4 IVAs en este formato.';
    begin
        TemplVATAmountLine.DeleteAll();
        prTCNLinGenericaVentas.SetRange("Document type", Rec."Document Type");
        prTCNLinGenericaVentas.SetRange("Document No.", Rec."No.");
        if prTCNLinGenericaVentas.FindSet(false) then begin
            repeat
                TemplVATAmountLine.Init();
                if Rec."Prices Including VAT" then begin
                    TemplVATAmountLine."Prices Including VAT" := true;
                end;
                TemplVATAmountLine.InsertLine();
            until prTCNLinGenericaVentas.Next() = 0;
        end;

        Clear(plTotalesImportesPie);
        Clear(plDatosImportesPie);
        plTotalFra := 0;
        xDimPorIVA := 1;
        xDimPorRE := 2;
        xDimImpLinea := 3;
        xDimDtoFra := 4;
        xDimDtoPP := 5;
        xDimBaseIVARE := 6;
        xDimImporteIVA := 7;
        xDimImporteRE := 8;
        i := 0;
        // *** Solo se imprimien 3 IVAS.
        if TemplVATAmountLine.Count > 4 then begin  //CAMBIAR EL 4
            Error(cImpresionFormatoIva);
        end;

        xDtoFra := "Invoice Discount Amount";
        i := 0;

        if TemplVATAmountLine.FindSet(false) then begin
            repeat
                i += 1;
                plDatosImportesPie[i, xDimPorIVA] := TemplVATAmountLine."VAT %";
                plDatosImportesPie[i, xDimPorRE] := TemplVATAmountLine."EC %";
                plDatosImportesPie[i, xDimImpLinea] := TemplVATAmountLine."Line Amount";
                if xDtoFra <> 0 then begin
                    plDatosImportesPie[i, xDimDtoFra] := xDtoFra;
                end else
                    if TemplVATAmountLine."Invoice Discount Amount" <> 0 then begin
                        plTotalesImportesPie[1, xDimDtoFra] += TemplVATAmountLine."Invoice Discount Amount";
                    end else begin
                        plTotalesImportesPie[1, xDimDtoFra] += TemplVATAmountLine."Line Amount" - TemplVATAmountLine."VAT Base";
                    end;
                plDatosImportesPie[i, xDimDtoPP] := TemplVATAmountLine."Pmt. Discount Amount";
                plDatosImportesPie[i, xDimBaseIVARE] := TemplVATAmountLine."VAT Base";
                plDatosImportesPie[i, xDimImporteIVA] := TemplVATAmountLine."VAT Amount";
                plDatosImportesPie[i, xDimImporteRE] := TemplVATAmountLine."EC Amount";
                plTotalesImportesPie[1, xDimImpLinea] += TemplVATAmountLine."Line Amount";
                plTotalesImportesPie[1, xDimDtoPP] += TemplVATAmountLine."Pmt. Discount Amount";
                plTotalesImportesPie[1, xDimBaseIVARE] += TemplVATAmountLine."VAT Base";
                plTotalesImportesPie[1, xDimImporteIVA] += TemplVATAmountLine."VAT Amount";
                plTotalesImportesPie[1, xDimImporteRE] += TemplVATAmountLine."EC Amount";
                plTotalFra += TemplVATAmountLine."Amount Including VAT";
            until (TemplVATAmountLine.Next() = 0);
        end;
        plTotalesImportesPie[1, xDimPorRE] := plTotalesImportesPie[1, xDimImporteIVA] + plTotalesImportesPie[1, xDimImporteRE];
        xDtoFra := 0;
        if plTotalesImportesPie[1, xDimDtoFra] <> 0 then begin
            xDtoFra := Round((plTotalesImportesPie[1, xDimDtoFra] * 100) / plTotalesImportesPie[1, xDimImpLinea], 1);
        end;
        prTCNLinGenericaVentas.SetRange("Document Type");
        prTCNLinGenericaVentas.SetRange("Document No.");
    end;

    /// <summary>
    /// CalcVencimientosF.
    /// </summary>
    /// <param name="pDatosVencimiento">VAR array[50, 4] of Text.</param>


    /// <summary>
    /// ComprobarDescuentoF.
    /// </summary>
    /// <param name="prTCNLinGenericaVentas">VAR Record PSRLinGenericaVentasC01.</param>
    /// <returns>Return variable xSalida of type Boolean.</returns>
    procedure ComprobarDescuentoF(var prTCNLinGenericaVentas: Record PSRLinGenericaVentasC01) xSalida: Boolean

    begin
        prTCNLinGenericaVentas.SetRange("Document Type", Rec."Document Type");
        prTCNLinGenericaVentas.SetRange("Document No.", Rec."No.");
        if prTCNLinGenericaVentas.FindSet(false) then begin
            repeat
                xSalida := prTCNLinGenericaVentas."Line Discount %" <> 0;
            until (prTCNLinGenericaVentas.Next() = 0) or xSalida;
        end;
        prTCNLinGenericaVentas.SetRange("Document Type");
        prTCNLinGenericaVentas.SetRange("Document No.");
    end;

    /// <summary>
    /// LiquidacionesDocuF.
    /// </summary>
    procedure LiquidacionesDocuF()
    begin
        int := 1;
        xTotalLiquidacion := 0;
        rCustLedgerEntry.SetRange("Document No.", Rec."No.");
        if rCustLedgerEntry.FindSet() then begin
            xNMovDocu := rCustLedgerEntry."Entry No.";
        end;
        Clear(rCustLedgerEntry);
        if xNMovDocu <> 0 then begin
            rCustLedgerEntry2.SetRange("Closed by Entry No.", xNMovDocu);
            rCustLedgerEntry2.SetAutoCalcFields("Amount (LCY)");
            if rCustLedgerEntry2.FindSet(false) then begin
                repeat
                    xLiquidaciones[1, int] := Format(rCustLedgerEntry2."Posting Date");

                    xLiquidaciones[2, int] := Format(rCustLedgerEntry2."Amount (LCY)");
                    xTotalLiquidacion += rCustLedgerEntry2."Amount (LCY)";
                    int += 1;
                until (rCustLedgerEntry2.Next() = 0) or (int > 4);
            end;
        end;
    end;

    /// <summary>
    /// PedirDatosRetencionesF.
    /// </summary>
    /// <param name="pDatosRetenciones">VAR list of [Text].</param>
    procedure PedirDatosRetencionesF(var pDatosRetenciones: list of [Text])
    begin
    end;
}


   

