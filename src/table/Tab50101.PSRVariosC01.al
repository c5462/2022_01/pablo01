table 50101 "PSRVariosC01"
{
    Caption = 'Varios';
    DataClassification = ToBeClassified;
    LookupPageId = PSRListadevariosC01;
    DrillDownPageId = PSRListadevariosC01;


    fields
    {
        field(1; PSRTipoC01; Enum PSRTipoC01)
        {
            Caption = 'Tipo';
            DataClassification = OrganizationIdentifiableInformation;
        }

        field(2; PSRCodigoC01; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; "PSRDescripciónC01"; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; PeriodoSegundaVacunacion; DateFormula)
        {
            Caption = 'PeriodoSegundaVacunacion';
            DataClassification =OrganizationIdentifiableInformation;
        }
        

    }
    keys
    {
        key(PK; PSRTipoC01, PSRCodigoC01)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(Dropdown; PSRCodigoC01, PSRDescripciónC01)
        {

        }
    }

    trigger OnDelete()
    begin
        Rec.TestField(Bloqueado, true);

    end;
}
