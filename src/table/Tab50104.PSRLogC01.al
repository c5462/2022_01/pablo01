table 50104 "PSRLogC01"
{
    Caption = 'Log';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; IdUnico; Guid)
        {
            Caption = 'IdUnico';
            DataClassification = SystemMetadata;

        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; IdUnico)
        {
            Clustered = true;
        }
        key(k2;SystemCreatedAt)
        {
            
        }
    }
    trigger OnInsert()
    begin

        if IsNullGuid(rec.IdUnico) then begin
            Rec.Validate(IdUnico,CreateGuid());
        end;
    end;
}
