table 50103 "PSRLinPlanVacunacionC01"
{
    Caption = 'Lineas Plan Vacunación';
    DataClassification = ToBeClassified;
    LookupPageId = PSRSubPagLinPlanVacunacionC01;
    DrillDownPageId = PSRSubPagLinPlanVacunacionC01;

    fields
    {
        field(1; CodigoLinea; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Numero Linea';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; "Codigo Cliente a Vacunar"; Code[20])
        {
            Caption = 'CodCteVacunar';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Customer."No.";

            trigger OnValidate()
            var
                rlPSRCabPlanVacunacionC01: Record PSRCabPlanVacunacionC01;

            begin
                if Rec.FechaVacuna = 0D then begin
                    if rlPSRCabPlanVacunacionC01.Get(Rec.CodigoLinea) then begin
                        Rec.Validate(FechaVacuna, rlPSRCabPlanVacunacionC01.FechaIniciovacunacionPlanificada);
                    end;
                end;
            end;
        }
        field(4; FechaVacuna; Date)
        {
            Caption = 'FechaVacuna';
            DataClassification = OrganizationIdentifiableInformation;
            trigger OnValidate()
            begin
                CalcularFechaProxVacunacionF();
            end;

        }
        field(5; CodigoVacuna; Code[20])
        {
            Caption = 'Codigo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = PSRVariosC01.PSRCodigoC01 where(PSRTipoC01 = const(Vacuna), Bloqueado = const(false));

            trigger OnValidate()
            begin
                CalcularFechaProxVacunacionF();
            end;
        }


        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Codigo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = PSRVariosC01.PSRCodigoC01 where(PSRTipoC01 = const(Otros), Bloqueado = const(false));
        }

        field(7; FechaProximaVacunacion; Date)
        {
            DataClassification = OrganizationIdentifiableInformation;
            Caption = 'Fecha Proxima Vacuna';
        }


    }
    keys
    {
        key(PK; CodigoLinea, NumLinea)
        {
            Clustered = true;
        }
    }

    procedure NombreClienteF(): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(Rec."Codigo Cliente a Vacunar") then begin
            exit(rlCustomer.Name);
        end;
    end;


    procedure DescripcionVariosF(ptipo: Enum PSRTipoC01; pCodVarios: Code[20]): Text
    var
        rlPSRVariosC01: Record PSRVariosC01;
    begin
        if rlPSRVariosC01.Get(ptipo, pCodVarios) then begin
            exit(rlPSRVariosC01."PSRDescripciónC01")
        end;
    end;

    local procedure CalcularFechaProxVacunacionF()
    var
        rlPSRVariosC01: Record PSRVariosC01;
        IsHandled: Boolean;
    begin
        OnBeforeCalcularFechaProxVacunaF(Rec, xRec, rlPSRVariosC01, IsHandled, CurrFieldNo);
        if not IsHandled then begin
            if Rec.FechaVacuna <> 0D then begin
                if rlPSRVariosC01.Get(rlPSRVariosC01.PSRTipoC01::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.Validate(FechaProximaVacunacion, CalcDate(rlPSRVariosC01.PeriodoSegundaVacunacion, Rec.FechaVacuna));
                end;
            end;
            OnAfterCalcularFechaProxVacunaF(Rec, xRec, rlPSRVariosC01, CurrFieldNo);
        end;
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProxVacunaF(Rec: Record PSRLinPlanVacunacionC01; xRec: Record PSRLinPlanVacunacionC01; rlPSRVariosC01: Record PSRVariosC01; var IsHandled: Boolean; pCurrFieldNo: Integer)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProxVacunaF(Rec: Record PSRLinPlanVacunacionC01; xRec: Record PSRLinPlanVacunacionC01; rlPSRVariosC01: Record PSRVariosC01; CurrFieldNo: Integer)
    begin
    end;




}
